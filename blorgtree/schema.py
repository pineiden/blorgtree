from dataclasses import dataclass, field, asdict
from typing import List, Optional, Dict, Any

@dataclass
class Schema:
    name:str
    files:List[str] = field(default_factory=list)
    directories:List[str] = field(default_factory=list)

    def dict(self):
        return asdict(self)
