def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return  "%3.1f %s" % (num, x)
        num /= 1024.0

def file_size(file_bytes):
    """
    this function will return the file size
    """
    return convert_bytes(file_bytes)
