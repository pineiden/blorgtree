from pathlib import Path
import yaml
from treedir import Builder, DirNodeDir, FileNodeDir, TreeDir
from rich import print
import time
from typing import List, Optional, Dict, Any
from dataclasses import dataclass, field, asdict
from functools import lru_cache
from .functions import file_size, convert_bytes
from .schema import Schema
from .extension import Extension
from .read_html import read, HTMLData

@dataclass
class BlorgTree:
    name: str
    config_path:Path
    tree:Optional[DirNodeDir] = None
    
    def __post_init__(self):
        CONFIG_TEXT = self.config_path.read_text()
        CONFIG_REPO = yaml.load(
            CONFIG_TEXT, 
            Loader=yaml.FullLoader)
        # Se crea nodo default
        # un nombre sería. "base"
        PROJECT_PATH = self.config_path.parent.absolute()
        self.main_path = PROJECT_PATH
        roots = PROJECT_PATH / CONFIG_REPO.get("org", None)
        leaves = PROJECT_PATH / CONFIG_REPO.get("public", None)
        schema = {
            "name": CONFIG_REPO.get("project")   
        }
        self.config = CONFIG_REPO
        orgs = Schema("org")
        htmls = Schema("html")
        directories = self.config.get("directories",[])
        directories.append("")
        directories_roots = [roots / d for d in directories]
        directories_leaves = [leaves / d for d in directories]
        self.org = self.config.get("org")
        self.public = self.config.get("public")
        # crear base node
        options = {}
        #create new class with associate config
        base = DirNodeDir(schema["name"], None, options=options)
        # build tree for org
        path = roots                                                                                            
        self.add_files(orgs, path, directories_roots, Extension("org"))
        # build tree for html
        path = leaves
        self.add_files(htmls, path, directories_leaves, Extension("html"))
        tree_org = Builder().do_tree(orgs.dict())
        for node in tree_org.children:
            if isinstance(node, TreeDir):
                node.set_parent(base)
        tree_html = Builder().do_tree(htmls.dict())
        for node in tree_html.children:
            if isinstance(node, TreeDir):
                node.set_parent(base)
        
        self.tree = base

    def add_files(
            self, 
            schema:Schema,
            path:Path,
            directories:List[Path],
            ext:Extension):
        for item in path.rglob(str(ext)):
            this_path = item.parent
            if this_path in directories:
                base_url = str(item.parent).replace(str(path.parent), "")
                # take the parents and splits in name for nodes
                # regulate the base
                parent = [d for d in base_url.split("/") if d]
                stats = path.stat() 
                fsize = file_size(stats.st_size)
                created = time.ctime(stats.st_mtime)
                node = {
                    "name": item.name,
                    "deep_path": parent,
                    "type":"file",
                    "options":{
                        "bytes": stats.st_size,
                        "size": fsize,
                        "created": created,
                        "url": f"{base_url}/{item.name}"
                    }
                }
                schema.files.append(node)

    def json(self, level=0):
        return self.tree.toJSON(level)


    @property
    def orgs(self):
        for ch in self.tree.children:
            if ch.name == self.org:
                return ch

    @property
    def htmls(self):
        for ch in self.tree.children:
            if ch.name == self.public:
                return ch

    def read_node(self, _id:str)->Dict[str, Any]:
        node = self.tree.get_node(_id)
        if isinstance(node, FileNodeDir):
            path = node.options.get("url")
            html_path = self.main_path.absolute() / path[1:] 
            if path.endswith("html"):
                html = read(html_path)
                return html
            return HTMLData()
        return HTMLData()
