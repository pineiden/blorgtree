import sys
from pathlib import Path
from blorgtree import BlorgTree
from rich import print

if __name__ == '__main__':
    config_path = Path(sys.argv[1])
    name = "org_blog"
    btree = BlorgTree(name, config_path)
    print(btree.json())
    for node in btree.tree.children:
        for node in node.children:
            idx = node.idx
            html = btree.read_node(idx)
            print("HTML", html)
