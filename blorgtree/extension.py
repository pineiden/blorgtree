from dataclasses import dataclass, field, asdict
from typing import List, Optional, Dict, Any

@dataclass
class Extension:
    name:str
    def __str__(self):
        return f"*.{self.name}"
