from setuptools import setup, find_packages
from pathlib import Path

path = Path(__file__).resolve().parent
README = path / "README.org"
text = README.read_text()

setup(name='blorgtree',
      version="15.06.2022",
      description='BlorgTree is a dual tree of nodes for Org with HTML projects ',
      url='https://gitlab.com/pineiden/blorgtree.git',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      install_requires=['click', 'anytree', "treedir"],
      scripts=[
      ],
      entry_points={
      },
      packages=find_packages(),
      include_package_data=True,
      license='GPLv3',
      long_description=text,
      long_description_content_type='text/markdown',
      zip_safe=False)
