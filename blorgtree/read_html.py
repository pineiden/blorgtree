from bs4 import BeautifulSoup
import sys
from pathlib import Path

from dataclasses import dataclass, asdict
from typing import List,Optional

@dataclass
class HTMLData:
    title:Optional[str] = None
    styles:Optional[List[str]] = None
    scripts:Optional[List[str]]= None
    content:Optional[str] = None

    def dict(self):
        return asdict(self)

def read(path:Path)->HTMLData:
    if path.exists():
        html = path.read_text()
        bs = BeautifulSoup(html, "html.parser")
        head = bs.head
        title = head.title.text
        styles = head.find_all("link")
        scripts = head.find_all("script")
        content = bs.body.find(id='content')
        return HTMLData(title=title, styles=styles, scripts=scripts, content=content)
    return HTMLData()
